package com.naiksan.inventorymanager.analytics

import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import javax.inject.Inject

private const val TAG = "AppAnalytics"

/**
 * Wrapper around Firebase Analytics. It can log button clicks, screen transitions, api failures
 */
class AppAnalytics @Inject constructor(
    private val analytics: FirebaseAnalytics
) {

    object EventType {
        const val SCREEN_TRANSITION = "screen_transition"
        const val BUTTON_CLICK = "button_click"
        const val API_FAILURE = "api_failure"
    }

    object ButtonType {
        const val EDIT_PRODUCTS = "edit_products"
    }

    companion object {
        /**
         * Extension function on view which logs click event using [description]
         */
        fun View.setOnClickListenerWithAnalytics(description: String, body: () -> Unit) {
            setOnClickListener {
                body.invoke()
            }
            Firebase.analytics.logEvent(EventType.BUTTON_CLICK, Bundle().apply {
                putString("description", description)
            })
        }
    }

    fun logScreenTransitionEvent(fromScreen: String, toScreen: String) {
        logEvent(EventType.SCREEN_TRANSITION, Bundle().apply {
            putString("from_screen", fromScreen)
            putString("to_screen", toScreen)
        })
    }

    fun logApiFailureEvent(
        method: String,
        route: String,
        params: String?,
        errorCode: String,
        errorMessage: String
    ) {
        logEvent(EventType.API_FAILURE, Bundle().apply {
            putString("method", method)
            putString("route", route)
            putString("params", params)
            putString("error_code", errorCode)
            putString("error_message", errorMessage)
        })
    }

    private fun logEvent(eventName: String, bundle: Bundle?) {
        analytics.logEvent(eventName, bundle)
    }

}