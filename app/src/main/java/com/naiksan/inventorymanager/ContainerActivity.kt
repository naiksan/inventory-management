package com.naiksan.inventorymanager

import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.naiksan.inventorymanager.analytics.AppAnalytics
import com.naiksan.inventorymanager.databinding.ActivityMainBinding
import com.naiksan.inventorymanager.ui.SharedViewModel
import com.naiksan.inventorymanager.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

private const val TAG = "ContainerActivity"

/**
 * Container Activity for all the fragments (Single activity architecture).
 *
 */
@AndroidEntryPoint
class ContainerActivity : BaseActivity() {
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!
    private lateinit var navController: NavController
    private var lastScreenName: String = ""

    @Inject
    lateinit var appAnalytics: AppAnalytics

    private val sharedViewModel by viewModels<SharedViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)
        listenToConnectivityChanges()
    }

    private val listener =
        NavController.OnDestinationChangedListener { _, destination, _ ->
            //Analytics example for screen transitions
            appAnalytics.logScreenTransitionEvent(
                fromScreen = lastScreenName,
                toScreen = destination.label?.toString() ?: ""
            )
            lastScreenName = destination.label?.toString() ?: ""
        }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(listener)
    }

    override fun onPause() {
        navController.removeOnDestinationChangedListener(listener)
        super.onPause()
    }

    private fun listenToConnectivityChanges() {
        sharedViewModel.networkState.observe(this, { available ->
            if (available) {
                setOnline()
            } else {
                setOffline()
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        return navController.navigateUp() || super.onSupportNavigateUp()
    }


    private fun setOffline() {
        binding.connectivityTv.text = getString(R.string.offline)
        binding.connectivityTv.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.offlineIndicator
            )
        )
    }

    private fun setOnline() {
        binding.connectivityTv.text = getString(R.string.online)
        binding.connectivityTv.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.onlineIndicator
            )
        )

    }


}