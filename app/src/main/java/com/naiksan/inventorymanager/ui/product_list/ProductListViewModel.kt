/**
 * @author sanket
 * Created on 16/04/21
 */

package com.naiksan.inventorymanager.ui.product_list

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.naiksan.inventorymanager.data.repository.product.ProductRepository
import com.naiksan.inventorymanager.model.InventoryProduct
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

private const val TAG = "ProductListViewModel"

@HiltViewModel
class ProductListViewModel @Inject constructor(
    private val repository: ProductRepository
) : ViewModel() {

    private var currentSearchResult: Flow<PagingData<InventoryProduct>>? = null

    var currentNameKeyword: String = ""
    var currentManufacturerKeyword: String = ""

    val selectedProducts = mutableListOf<InventoryProduct>()

    lateinit var mode: ProductListFragmentMode
        private set

    fun setMode(mode: ProductListFragmentMode) {
        this.mode = mode
    }

    init {
        resetEditMode()
    }

    /**
     * This reads only from the db products, as new products gets added to inventory, this will be populated
     */
    fun getManufacturersStream(): Flow<List<String>> {
        return repository.getManufacturers()
    }


    fun fetch(name: String, manufacturer: String): Flow<PagingData<InventoryProduct>> {
        val lastResult = currentSearchResult
        if (currentNameKeyword == name && currentManufacturerKeyword == manufacturer && lastResult != null) {
            return lastResult
        }
        currentNameKeyword = name
        currentManufacturerKeyword = manufacturer
        return if (mode == ProductListFragmentMode.SEARCH) {
            search()
        } else {
            fetchFromDb()
        }
    }

    private fun fetchFromDb(
    ): Flow<PagingData<InventoryProduct>> {
        val newResult: Flow<PagingData<InventoryProduct>> =
            repository.getInventoryProducts(currentNameKeyword, currentManufacturerKeyword)
                .map { pagingData ->
                    pagingData.map {
                        InventoryProduct(
                            id = it.id,
                            name = it.name,
                            manufacturer = it.manufacturer,
                            sku = it.sku,
                            unitQty = it.unitQty,
                            unit = it.unit,
                            qty = it.qty,
                        )
                    }
                }
                .cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }

    /**
     * Db is treated as single source of truth,
     * Network request is made and on success data is cached into [SearchProductDao]
     */
    private fun search(): Flow<PagingData<InventoryProduct>> {
        val newResult: Flow<PagingData<InventoryProduct>> =
            repository.searchProducts(currentNameKeyword, currentManufacturerKeyword)
                .map { pagingData ->
                    pagingData.map {
                        InventoryProduct(
                            id = it.id,
                            name = it.name,
                            manufacturer = it.manufacturer,
                            sku = it.sku,
                            unitQty = it.unitQty,
                            unit = it.unit,
                            qty = it.qty ?: 0,
                        )
                    }
                }
                .cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }

    fun selectItem(productEntity: InventoryProduct) {
        selectedProducts.add(productEntity)
    }

    fun deselectItem(productEntity: InventoryProduct) {
        selectedProducts.remove(productEntity)
    }

    fun resetEditMode() {
        selectedProducts.clear()
    }
}