/**
 * @author sanket
 * Created on 27/11/20
 */

package com.naiksan.inventorymanager.ui.base

import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment


open class BaseFragment : Fragment() {
    override fun onStop() {
        super.onStop()
        val imm: InputMethodManager? =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.let { manager ->
            if (manager.isAcceptingText) {
                manager.hideSoftInputFromWindow(requireActivity().currentFocus?.windowToken, 0)
            }
        }

    }
}