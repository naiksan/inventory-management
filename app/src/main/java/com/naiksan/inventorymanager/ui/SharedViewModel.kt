/**
 * @author sanket
 */

package com.naiksan.inventorymanager.ui

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.naiksan.inventorymanager.data.repository.product.ProductRepository
import com.naiksan.inventorymanager.model.InventoryProduct
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

private const val TAG = "SharedViewModel"

/**
 * SharedViewModel is used for listening to connectivity change. It also provides livedata which can be observed in UI component.
 * It also holds the products shown in edit screen
 */
@HiltViewModel
class SharedViewModel @Inject constructor(
    @ApplicationContext context: Context,
    private val repository: ProductRepository
) : ViewModel() {
    private val connectivityManager by lazy { context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }

    private var networkCallback: ConnectivityManager.NetworkCallback? = null

    val editProductList = mutableListOf<InventoryProduct>()
    private val deletedProducts = mutableSetOf<InventoryProduct>()

    private val _networkState = MutableLiveData<Boolean>()
    val networkState: LiveData<Boolean> = _networkState

    fun addDeletedProduct(product: InventoryProduct) {
        deletedProducts.add(product)
    }

    fun addInventoryProducts(list: List<InventoryProduct>) {
        list.forEach {
            addInventoryProduct(it)
        }
    }

    fun addInventoryProduct(product: InventoryProduct) {
        val p = editProductList.firstOrNull { it.id == product.id }
        if (p == null) {
            editProductList.add(product)
            deletedProducts.remove(product)
        }
    }

    fun clearInventoryProducts() = editProductList.clear()

    suspend fun deleteProducts() {
        deletedProducts.forEach {
            repository.deleteProduct(it.id)
        }
    }

    init {
        registerConnectivityMonitoring()
    }

    private fun registerConnectivityMonitoring() {
        val networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                setOnline()
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                setOffline()
            }

            override fun onUnavailable() {
                super.onUnavailable()
                setOffline()
            }

        }
        this.networkCallback = networkCallback
        connectivityManager.registerNetworkCallback(
            NetworkRequest.Builder().build(),
            networkCallback
        )
    }

    fun setOnline() {
        _networkState.postValue(true)
    }

    fun setOffline() {
        _networkState.postValue(false)
    }

    override fun onCleared() {
        super.onCleared()
        unregisterConnectivityMonitoring()
    }

    private fun unregisterConnectivityMonitoring() {
        val networkCallback = this.networkCallback ?: return
        connectivityManager.unregisterNetworkCallback(networkCallback)
        this.networkCallback = null
    }
}