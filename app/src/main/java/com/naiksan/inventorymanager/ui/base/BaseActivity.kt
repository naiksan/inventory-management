/**
 * @author sanket
 * Created on 27/11/20
 */

package com.naiksan.inventorymanager.ui.base

import androidx.appcompat.app.AppCompatActivity

open class BaseActivity: AppCompatActivity() {
}