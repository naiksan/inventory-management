/**
 * @author sanket
 * Created on 16/04/21
 */

package com.naiksan.inventorymanager.ui.edit

import androidx.lifecycle.ViewModel
import com.naiksan.inventorymanager.data.db.entities.UnitConverter
import com.naiksan.inventorymanager.data.network.dtos.ProductDTO
import com.naiksan.inventorymanager.data.network.response.Resource
import com.naiksan.inventorymanager.data.repository.product.ProductRepository
import com.naiksan.inventorymanager.model.InventoryProduct
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class EditProductsViewModel @Inject constructor(
    private val repository: ProductRepository
) : ViewModel() {

    var rawSkuCode: String? = null
    var rawqty: String? = null

    suspend fun saveProducts(list: List<InventoryProduct>): Boolean {
        return repository.saveToStock(
            list
                .map { it.toInventoryProductEntity() })
    }

    suspend fun deleteProduct(id: String): Boolean {
        return repository.deleteProduct(id)
    }

    suspend fun searchProductWithSku(): InventoryProduct? {
        val code = rawSkuCode
        if (code.isNullOrEmpty()) {
            return null
        }
        val result = repository.getProductWithSkuCode(code)
        if (result is Resource.Success) {
            val dto: ProductDTO = result.data
            return InventoryProduct(
                id = dto.id,
                name = dto.name,
                manufacturer = dto.manufacturer,
                sku = dto.sku,
                unitQty = dto.unitQty,
                unit = UnitConverter.productUnitFromString(dto.unit),
                qty = 0,
            )
        }
        return null
    }


}