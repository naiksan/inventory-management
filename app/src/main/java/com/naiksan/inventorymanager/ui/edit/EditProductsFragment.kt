package com.naiksan.inventorymanager.ui.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.naiksan.inventorymanager.databinding.FragmentProductEditListBinding
import com.naiksan.inventorymanager.ui.base.BaseFragment
import com.naiksan.inventorymanager.ui.SharedViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

private const val TAG = "EditProductsFragment"

@AndroidEntryPoint
class EditProductsFragment : BaseFragment() {
    private val viewModel: EditProductsViewModel by viewModels()
    private val sharedViewModel: SharedViewModel by activityViewModels()
    private val adapter: EditProductAdapter = EditProductAdapter()
    private var _binding: FragmentProductEditListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductEditListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkForBackPress()
        initViews()
        populateList()
    }

    private fun checkForBackPress() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    sharedViewModel.clearInventoryProducts()
                    isEnabled = false
                    (requireActivity() as? AppCompatActivity)?.onSupportNavigateUp()
                }
            })
    }

    private fun populateList() {
        adapter.submitList(sharedViewModel.editProductList)
    }

    private fun initViews() {
        binding.rv.adapter = adapter
        binding.skuEt.setOnEditorActionListener { _, actionId, _ ->
            if (EditorInfo.IME_ACTION_DONE == actionId) {
                viewModel.rawSkuCode = binding.skuEt.text.toString().trim()
                trySearchProduct()
            }
            false
        }

        binding.qtyEt.setOnEditorActionListener { _, actionId, _ ->
            if (EditorInfo.IME_ACTION_DONE == actionId) {
                viewModel.rawqty = binding.qtyEt.text.toString().trim()
                trySearchProduct()
            }
            false
        }

        adapter.onItemDelete { inventoryProduct, position ->
            sharedViewModel.editProductList.remove(inventoryProduct)
            adapter.remoteItem(position)
            sharedViewModel.addDeletedProduct(inventoryProduct)
        }
        binding.searchBtn.setOnClickListener {
            val direction =
                EditProductsFragmentDirections.actionEditProductsFragmentToSearchProductListFragment()
            findNavController().navigate(direction)
        }
        binding.saveBtn.setOnClickListener {
            lifecycleScope.launch {
                viewModel.saveProducts(sharedViewModel.editProductList)
                sharedViewModel.deleteProducts()
                sharedViewModel.clearInventoryProducts()
                findNavController().popBackStack()
            }
        }
        binding.doneBtn.setOnClickListener { trySearchProduct() }
    }

    private fun trySearchProduct() {
        if (sharedViewModel.networkState.value == true) {
            lifecycleScope.launch {
                viewModel.searchProductWithSku()?.let { product ->
                    binding.skuEt.text = null
                    sharedViewModel.addInventoryProduct(product)
                    adapter.notifyDataSetChanged()
                }
            }
        } else {
            Toast.makeText(requireContext(), "No internet connection", Toast.LENGTH_SHORT).show()
        }
    }
}