package com.naiksan.inventorymanager.ui.product_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.naiksan.inventorymanager.databinding.ItemInventoryProductBinding
import com.naiksan.inventorymanager.model.InventoryProduct

class ProductAdapter :
    PagingDataAdapter<InventoryProduct, RecyclerView.ViewHolder>(PRODUCT_COMPARATOR) {

    var selectedProducts = listOf<InventoryProduct>()

    var onItemSelected: ((InventoryProduct) -> Unit)? = null
    var onItemDeSelected: ((InventoryProduct) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding =
            ItemInventoryProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repoItem = getItem(position)
        if (repoItem != null) {
            (holder as ViewHolder).bind(repoItem)
        }
    }

    companion object {
        private val PRODUCT_COMPARATOR =
            object : DiffUtil.ItemCallback<InventoryProduct>() {
                override fun areItemsTheSame(
                    oldItem: InventoryProduct,
                    newItem: InventoryProduct
                ): Boolean =
                    oldItem.id == newItem.id

                override fun areContentsTheSame(
                    oldItem: InventoryProduct,
                    newItem: InventoryProduct
                ): Boolean =
                    oldItem == newItem
            }
    }

    inner class ViewHolder(private val binding: ItemInventoryProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: InventoryProduct) {
            binding.productNameTv.text =
                "${product.name} - ${product.unitQty}${product.unit.name.toLowerCase()}"
            binding.manufacturerTv.text = product.manufacturer
            binding.qtyTv.text = if (product.qty <= 0) "--" else product.qty.toString()
            binding.root.isSelected = selectedProducts.contains(product)
            binding.root.setOnClickListener {
                if (selectedProducts.contains(product)) {
                    onItemDeSelected?.invoke(product)
                } else {
                    onItemSelected?.invoke(product)
                }
                notifyItemChanged(absoluteAdapterPosition)
            }
        }
    }
}