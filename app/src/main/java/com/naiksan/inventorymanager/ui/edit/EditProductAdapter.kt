package com.naiksan.inventorymanager.ui.edit

import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.naiksan.inventorymanager.databinding.ItemProductEditBinding
import com.naiksan.inventorymanager.model.InventoryProduct

class EditProductAdapter :
    ListAdapter<InventoryProduct, RecyclerView.ViewHolder>(PRODUCT_COMPARATOR) {

    private var onItemDelete: ((InventoryProduct, Int) -> Unit)? = null
    fun onItemDelete(onItemDelete: ((InventoryProduct, Int) -> Unit)) {
        this.onItemDelete = onItemDelete
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding =
            ItemProductEditBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repoItem = getItem(position)
        if (repoItem != null) {
            (holder as ViewHolder).bind(repoItem)
        }
    }

    fun remoteItem(position: Int) {
        notifyItemRemoved(position)
    }

    companion object {
        private val PRODUCT_COMPARATOR = object : DiffUtil.ItemCallback<InventoryProduct>() {
            override fun areItemsTheSame(
                oldItem: InventoryProduct,
                newItem: InventoryProduct
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: InventoryProduct,
                newItem: InventoryProduct
            ): Boolean =
                oldItem == newItem
        }
    }

    inner class ViewHolder(private val binding: ItemProductEditBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: InventoryProduct) {
            if (product is InventoryProduct) {
                binding.productNameTv.text =
                    "${product.name} - ${product.unitQty}${product.unit.name.toLowerCase()}"

                binding.manufacturerTv.text = product.manufacturer
                binding.skuEt.setText(product.sku)
                binding.qtyEt.setText(if (product.qty <= 0) "" else product.qty.toString())
                binding.deleteBtn.isVisible = true
                binding.deleteBtn.setOnClickListener {
                    onItemDelete?.invoke(product, absoluteAdapterPosition)
                }
                binding.skuEt.inputType = InputType.TYPE_NULL
                binding.skuEt.isFocusable = false
                if (binding.qtyEt.tag != null && binding.qtyEt.tag is TextWatcher) {
                    binding.qtyEt.removeTextChangedListener(binding.qtyEt.tag as TextWatcher)
                }
                binding.qtyEt.setText(product.qty.toString())
                binding.qtyEt.tag = binding.qtyEt.addTextChangedListener {
                    product.qty = try {
                        binding.qtyEt.text.trim().toString().toInt()
                    } catch (e: Exception) {
                        0
                    }
                }
            }
        }
    }
}