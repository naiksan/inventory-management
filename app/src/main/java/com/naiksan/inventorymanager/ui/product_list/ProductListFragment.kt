package com.naiksan.inventorymanager.ui.product_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import com.naiksan.inventorymanager.analytics.AppAnalytics
import com.naiksan.inventorymanager.analytics.AppAnalytics.Companion.setOnClickListenerWithAnalytics
import com.naiksan.inventorymanager.databinding.FragmentProductListBinding
import com.naiksan.inventorymanager.ui.SharedViewModel
import com.naiksan.inventorymanager.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

enum class ProductListFragmentMode { VIEW, SEARCH }

private const val TAG = "ProductListFragment"

/***
 * This screen is capable of displaying online(search) as well as offline(db) data.
 * The behaviour can be changed using [ProductListFragmentArgs.Mode] property
 */
@AndroidEntryPoint
class ProductListFragment : BaseFragment() {
    private val viewModel: ProductListViewModel by viewModels()
    private val sharedViewModel: SharedViewModel by activityViewModels()
    private val adapter: ProductAdapter = ProductAdapter()
    private var _binding: FragmentProductListBinding? = null
    private val binding get() = _binding!!
    private var searchJob: Job? = null
    private var dropdownJob: Job? = null
    private val args by navArgs<ProductListFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setMode(args.mode)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        updateListFromInput()
        setUpManufacturerDropDown()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun updateListFromInput() {
        search(
            name = binding.keywordEt.text.toString().trim(),
            manufacturer = binding.manufacturerEt.text.toString().trim()
        )
    }

    override fun onPause() {
        super.onPause()
        adapter.removeLoadStateListener(stateListener)
        dropdownJob?.cancel()
    }

    private fun setUpManufacturerDropDown() {
        val et = binding.manufacturerEt as? AutoCompleteTextView
        dropdownJob?.cancel()
        dropdownJob = lifecycleScope.launch {
            viewModel.getManufacturersStream().collectLatest {
                val list = it
                if (list.isNullOrEmpty())
                    return@collectLatest

                val adapter = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_spinner_dropdown_item,
                    it
                )
                et?.apply {
                    setAdapter(adapter)
                    onItemClickListener = AdapterView.OnItemClickListener { _, _, _, _ ->
                        updateListFromInput()
                    }
                }

            }
        }
    }

    private fun initViews() {
        //Analytics example for button click
        binding.actionButton.setOnClickListenerWithAnalytics(description = AppAnalytics.ButtonType.EDIT_PRODUCTS) {
            sharedViewModel.addInventoryProducts(viewModel.selectedProducts)
            viewModel.resetEditMode()
            when (viewModel.mode) {
                ProductListFragmentMode.VIEW -> {
                    val direction =
                        ProductListFragmentDirections.actionInventoryProductListFragmentToEditProductsFragment()
                    findNavController().navigate(direction)
                }
                ProductListFragmentMode.SEARCH -> {
                    findNavController().popBackStack()
                }
            }
        }
        adapter.selectedProducts = viewModel.selectedProducts
        adapter.onItemSelected = { product ->
            viewModel.selectItem(product)
        }

        adapter.onItemDeSelected = { product ->
            viewModel.deselectItem(product)
        }
        binding.keywordEt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                updateListFromInput()
            }
            true
        }
        binding.rv.adapter = adapter.withLoadStateHeaderAndFooter(
            header = ListLoadStateAdapter { adapter.retry() },
            footer = ListLoadStateAdapter { adapter.retry() }
        )
        binding.actionButton.text = when (viewModel.mode) {
            ProductListFragmentMode.SEARCH -> "SELECT"
            ProductListFragmentMode.VIEW -> "EDIT"
        }
        adapter.addLoadStateListener(stateListener)
    }

    private val stateListener = { loadState: CombinedLoadStates ->

        binding.progressBar.isVisible = loadState.mediator?.refresh is LoadState.Loading

        binding.retryBtn.isVisible = loadState.mediator?.refresh is LoadState.Error

        val errorState = loadState.source.append as? LoadState.Error
            ?: loadState.source.prepend as? LoadState.Error
            ?: loadState.append as? LoadState.Error
            ?: loadState.prepend as? LoadState.Error

        errorState?.let {
            Toast.makeText(
                requireActivity(),
                "Error occurred",
                Toast.LENGTH_LONG
            ).show()
        }
        Unit
    }

    private fun search(name: String, manufacturer: String) {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.fetch(name, manufacturer).collectLatest {
                adapter.submitData(it)
            }
        }
    }
}