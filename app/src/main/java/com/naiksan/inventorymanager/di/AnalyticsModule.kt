/**
 * @author sanket
 * Created on 21/11/20
 */

package com.naiksan.inventorymanager.di

import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.naiksan.inventorymanager.analytics.AppAnalytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AnalyticsModule {

    @Singleton
    @Provides
    internal fun provideAppAnalytics(
        analytics: FirebaseAnalytics
    ): AppAnalytics =
        AppAnalytics(analytics = analytics)

    @Singleton
    @Provides
    internal fun provideAnalytics(): FirebaseAnalytics =
        Firebase.analytics

}