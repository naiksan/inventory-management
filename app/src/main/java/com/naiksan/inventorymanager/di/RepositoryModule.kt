package com.naiksan.inventorymanager.di

import com.naiksan.inventorymanager.data.db.AppDatabase
import com.naiksan.inventorymanager.data.network.ApiService
import com.naiksan.inventorymanager.data.repository.product.ProductRepository
import com.naiksan.inventorymanager.data.repository.product.ProductRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    internal fun provideProductRepository(
        service: ApiService,
        db: AppDatabase,
    ): ProductRepository =
        ProductRepositoryImpl(
            service,
            db,
        )
}