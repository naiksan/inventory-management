/**
 * @author sanket
 * Created on 21/11/20
 */

package com.naiksan.inventorymanager.di

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
object CoreApplicationModule