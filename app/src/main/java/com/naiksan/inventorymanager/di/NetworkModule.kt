/**
 * @author sanket
 * Created on 21/11/20
 */

package com.naiksan.inventorymanager.di

import android.content.Context
import com.naiksan.inventorymanager.BuildConfig
import com.naiksan.inventorymanager.analytics.AppAnalytics
import com.naiksan.inventorymanager.data.network.ApiService
import com.naiksan.inventorymanager.data.network.interceptor.MockInterceptor
import com.naiksan.inventorymanager.data.network.interceptor.NetworkInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    @Singleton
    internal fun provideCoreApiService(
        retrofit: Retrofit
    ): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        @Named("MockInterceptor") mockInterceptor: Interceptor,
        @Named("NetworkInterceptor") networkInterceptor: Interceptor,
    ): OkHttpClient {
        return OkHttpClient
            .Builder()
            .addInterceptor(mockInterceptor)
            .addInterceptor(networkInterceptor)
            .build()
    }

    @Provides
    @Singleton
    internal fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: Converter.Factory,
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.API_BASE_URL)
            .addConverterFactory(converterFactory)
            .build()
    }

    @Provides
    @Singleton
    @Named("MockInterceptor")
    fun provideMockInterceptor(
        @ApplicationContext appContext: Context
    ): Interceptor = MockInterceptor(appContext)

    @Provides
    @Singleton
    @Named("NetworkInterceptor")
    fun provideNetworkInterceptor(
        appAnalytics: AppAnalytics
    ): Interceptor = NetworkInterceptor(appAnalytics)
}