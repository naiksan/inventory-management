package com.naiksan.inventorymanager.model

import com.naiksan.inventorymanager.data.db.entities.InventoryProductEntity
import com.naiksan.inventorymanager.data.db.entities.ProductUnit

/**
 * UI level model, used in List Adapters
 */
data class InventoryProduct(
    val id: String,
    val name: String,
    val manufacturer: String,
    val sku: String,
    val unitQty: Double,
    val unit: ProductUnit,
    var qty: Int,
) {
    fun toInventoryProductEntity(): InventoryProductEntity {
        return InventoryProductEntity(
            id = id,
            name = name,
            manufacturer = manufacturer,
            sku = sku,
            unitQty = unitQty,
            unit = unit,
            qty = qty
        )
    }
}