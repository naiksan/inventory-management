package com.naiksan.inventorymanager.data.network.dtos

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
class ProductDTO(
    @SerializedName("id")
    val id: String,
    @SerializedName("manufacturer")
    val manufacturer: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("sku")
    val sku: String,
    @SerializedName("unit")
    val unit: String,
    @SerializedName("unitQty")
    val unitQty: Double
)