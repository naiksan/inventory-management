/**
 * @author sanket
 * Created on 21/11/20
 */

package com.naiksan.inventorymanager.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.naiksan.inventorymanager.data.db.daos.InventoryProductDao
import com.naiksan.inventorymanager.data.db.daos.RemoteKeysDao
import com.naiksan.inventorymanager.data.db.daos.SearchProductDao
import com.naiksan.inventorymanager.data.db.entities.InventoryProductEntity
import com.naiksan.inventorymanager.data.db.entities.RemoteKeys
import com.naiksan.inventorymanager.data.db.entities.SearchProductEntity
import com.naiksan.inventorymanager.data.db.entities.UnitConverter

private const val TAG = "AppDatabase"

@Database(
    entities = [InventoryProductEntity::class, SearchProductEntity::class, RemoteKeys::class],
    version = 1
)
@TypeConverters(UnitConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun inventoryProductDao(): InventoryProductDao
    abstract fun searchProductDao(): SearchProductDao
    abstract fun remoteKeysDao(): RemoteKeysDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, "inventory_manager.db"
            ).build()
    }
}