/**
 * @author sanket
 * Created on 11/12/20
 */

package com.naiksan.inventorymanager.data.network.response

import retrofit2.HttpException
import java.io.EOFException
import java.net.HttpURLConnection
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException


data class ApiResponse<T>(
    val data: T?,
    val error: Error.ApiError?
)

sealed class Resource<out T> {
    data class Success<out T>(val data: T) : Resource<T>()
    data class Failure(val error: Error) : Resource<Nothing>()
}

sealed class Error : Exception() {
    object NoInternetConnection : Error()
    object Timeout : Error()
    sealed class NetworkError : Error() {
        companion object
        object Unauthorized : NetworkError()
        object Forbidden : NetworkError()
        object NotFound : NetworkError()
        object InvalidResponse : NetworkError()
        object Other : NetworkError()
    }

    data class ApiError(
        val code: String,
        override val message: String
    ) : Error()
}

fun Exception.toError(): Resource.Failure {
    return when (this) {
        is UnknownHostException -> Resource.Failure(Error.NoInternetConnection)
        is TimeoutException -> Resource.Failure(Error.Timeout)
        is HttpException -> {
            when (this.code()) {
                HttpURLConnection.HTTP_UNAUTHORIZED -> Resource.Failure(Error.NetworkError.Unauthorized)
                HttpURLConnection.HTTP_FORBIDDEN -> Resource.Failure(Error.NetworkError.Forbidden)
                HttpURLConnection.HTTP_NOT_FOUND -> Resource.Failure(Error.NetworkError.NotFound)
                else -> Resource.Failure(Error.NetworkError.Other)
            }
        }
        is EOFException -> Resource.Failure(Error.NetworkError.InvalidResponse)
        else -> Resource.Failure(Error.NetworkError.Other)
    }
}