package com.naiksan.inventorymanager.data.repository.product

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.naiksan.inventorymanager.data.db.AppDatabase
import com.naiksan.inventorymanager.data.db.entities.InventoryProductEntity
import com.naiksan.inventorymanager.data.db.entities.SearchProductEntity
import com.naiksan.inventorymanager.data.network.ApiService
import com.naiksan.inventorymanager.data.network.dtos.ProductDTO
import com.naiksan.inventorymanager.data.network.response.Error
import com.naiksan.inventorymanager.data.network.response.Resource
import com.naiksan.inventorymanager.data.network.response.toError
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

object PageSize {
    const val NETWORK_PAGE_SIZE = 10
}

private const val TAG = "ProductRepositoryImpl"

class ProductRepositoryImpl @Inject constructor(
    private val service: ApiService,
    private val db: AppDatabase,
) : ProductRepository {
    override fun getInventoryProducts(
        name: String?,
        manufacturer: String?,
    ): Flow<PagingData<InventoryProductEntity>> {
        val nameQuery = "%${name ?: ""}%"
        val manufacturerQuery = "%${manufacturer ?: ""}%"

        val pagingSourceFactory =
            { db.inventoryProductDao().getInventoryBy(nameQuery, manufacturerQuery) }
        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(
                pageSize = PageSize.NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    override suspend fun saveToStock(list: List<InventoryProductEntity>): Boolean {
        val result = db.inventoryProductDao().insert(list)
        return result.isNotEmpty()
    }

    override suspend fun deleteProduct(id: String): Boolean {
        db.inventoryProductDao().delete(id)
        return true
    }

    override fun searchProducts(
        name: String?,
        manufacturer: String?,
    ): Flow<PagingData<SearchProductEntity>> {
        val nameQuery = "%${name ?: ""}%"
        val manufacturerQuery = "%${manufacturer ?: ""}%"
        val pagingSourceFactory = {
            db.searchProductDao().getSearch(
                nameQuery,
                manufacturerQuery
            )
        }
        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(
                pageSize = PageSize.NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = pagingSourceFactory,
            remoteMediator = ProductsMediator(
                service = service,
                db = db,
                name = name,
                manufacturer = manufacturer
            )
        ).flow
    }

    override suspend fun getProductWithSkuCode(skuCode: String): Resource<ProductDTO> {
        return try {
            val response = service.getProductBySkuAsync(skuCode)
            val error = response.error
            val data = response.data
            when {
                error != null -> Resource.Failure(error)
                data != null -> Resource.Success(response.data.product)
                else -> Resource.Failure(Error.NetworkError.Other)
            }
        } catch (e: Exception) {
            e.toError()
        }
    }

    override fun getManufacturers(): Flow<List<String>> {
        return db.inventoryProductDao().getManufactures()
    }
}