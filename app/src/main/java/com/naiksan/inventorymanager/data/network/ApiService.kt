/**
 * @author sanket
 * Created on 21/11/20
 */

package com.naiksan.inventorymanager.data.network

import com.naiksan.inventorymanager.data.network.response.ApiResponse
import com.naiksan.inventorymanager.data.network.response.GetProductResponse
import com.naiksan.inventorymanager.data.network.response.SearchProductsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/products")
    suspend fun searchProductsAsync(
        @Query("name")
        name: String? = null,
        @Query("manufacturer")
        manufacturer: String? = null,
        @Query("page")
        page: Int,
    ): ApiResponse<SearchProductsResponse>

    @GET("/product")
    suspend fun getProductBySkuAsync(
        @Query("sku")
        sku: String,
    ): ApiResponse<GetProductResponse>

}