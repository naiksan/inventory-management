/**
 * @author sanket
 * Created on 11/12/20
 */

package com.naiksan.inventorymanager.data.network.response

import androidx.annotation.Keep

import com.google.gson.annotations.SerializedName
import com.naiksan.inventorymanager.data.network.dtos.ProductDTO

@Keep
data class SearchProductsResponse(
    @SerializedName("products")
    val products: List<ProductDTO>
)