/**
 * @author sanket
 * Created on 23/11/20
 */

package com.naiksan.inventorymanager.data.repository.product

import androidx.paging.PagingData
import com.naiksan.inventorymanager.data.db.entities.InventoryProductEntity
import com.naiksan.inventorymanager.data.db.entities.SearchProductEntity
import com.naiksan.inventorymanager.data.network.dtos.ProductDTO
import com.naiksan.inventorymanager.data.network.response.Resource
import kotlinx.coroutines.flow.Flow

interface ProductRepository {
    fun getInventoryProducts(
        name: String?,
        manufacturer: String?,
    ): Flow<PagingData<InventoryProductEntity>>

    suspend fun saveToStock(list: List<InventoryProductEntity>): Boolean
    suspend fun deleteProduct(id: String): Boolean
    fun searchProducts(
        name: String?,
        manufacturer: String?,
    ): Flow<PagingData<SearchProductEntity>>

    suspend fun getProductWithSkuCode(skuCode: String): Resource<ProductDTO>

    fun getManufacturers(): Flow<List<String>>
}