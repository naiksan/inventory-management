package com.naiksan.inventorymanager.data.network.interceptor

import android.content.Context
import android.util.Log
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.*
import java.io.IOException
import javax.inject.Inject


private const val TAG = "MockInterceptor"

/**
 * Mock API requests
 */
class MockInterceptor @Inject constructor(
    @ApplicationContext val context: Context
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        try {
            val path = chain.request().url().uri().path
            //returns pair of response & code
            val responseString = when {
                path.endsWith("/products") -> {
                    val page: String? =
                        chain.request().url().queryParameter("page")

                    getJsonDataFromAsset("search_products$page.json") to 200
                }

                path.endsWith("/product") -> {
                    getJsonDataFromAsset("search_product_by_sku.json") to 200
                }
                else -> getJsonDataFromAsset("response/not_found.json") to 404
            }

            return chain.proceed(chain.request())
                .newBuilder()
                .code(responseString.second)
                .protocol(Protocol.HTTP_2)
                .message(responseString.first)
                .body(
                    ResponseBody.create(
                        MediaType.parse("application/json"),
                        responseString.first.toByteArray()
                    )
                )
                .addHeader("content-type", "application/json")
                .build()
        } catch (e: Exception) {
            return chain.proceed(chain.request())
        }

    }

    private fun getJsonDataFromAsset(fileName: String): String {
        return try {
            context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            ""
        }
    }


}
