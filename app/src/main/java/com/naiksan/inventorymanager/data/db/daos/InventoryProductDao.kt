/**
 * @author sanket
 * Created on 21/11/20
 */

package com.naiksan.inventorymanager.data.db.daos

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naiksan.inventorymanager.data.db.entities.InventoryProductEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface InventoryProductDao {

    @Query("SELECT * FROM inventory_product WHERE name LIKE :name AND manufacturer LIKE :manufacturer ORDER BY id DESC")
    fun getInventoryBy(
        name: String,
        manufacturer: String
    ): PagingSource<Int, InventoryProductEntity>

    @Query("SELECT DISTINCT inventory_product.manufacturer FROM inventory_product ORDER BY inventory_product.manufacturer DESC")
    fun getManufactures(): Flow<List<String>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(products: List<InventoryProductEntity>): List<Long>

    @Query("DELETE FROM inventory_product WHERE inventory_product.id = :id")
    suspend fun delete(id: String)
}
