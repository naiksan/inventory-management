/**
 * @author sanket
 */

package com.naiksan.inventorymanager.data.network.interceptor

import android.util.Log
import com.naiksan.inventorymanager.analytics.AppAnalytics
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.net.HttpURLConnection.HTTP_OK
import javax.inject.Inject


private const val TAG = "NetworkInterceptor"

/**
 *  Logs API errors when error occurs
 */
class NetworkInterceptor @Inject constructor(
    private val appAnalytics: AppAnalytics
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val response = chain.proceed(request)
        if (response.code() != HTTP_OK) {
            //Analytics example for network error logging
            appAnalytics.logApiFailureEvent(
                method = request.method(),
                route = request.url().toString(),
                params = request.body()?.toString(),
                errorCode = response.code().toString(),
                errorMessage = response.body()?.string() ?: "Uknown error"
            )
        }
        return response
    }
}