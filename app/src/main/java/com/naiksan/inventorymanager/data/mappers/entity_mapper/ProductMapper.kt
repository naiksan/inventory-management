package com.naiksan.inventorymanager.data.mappers.entity_mapper

import com.naiksan.inventorymanager.data.db.entities.SearchProductEntity
import com.naiksan.inventorymanager.data.db.entities.UnitConverter
import com.naiksan.inventorymanager.data.network.dtos.ProductDTO

object ProductMapper {
    fun dtoToEntity(dto: ProductDTO): SearchProductEntity {
        return SearchProductEntity(
            id = dto.id,
            name = dto.name,
            manufacturer = dto.manufacturer,
            sku = dto.sku,
            unitQty = dto.unitQty,
            unit = UnitConverter.productUnitFromString(dto.unit),
            qty = null
        )
    }
}