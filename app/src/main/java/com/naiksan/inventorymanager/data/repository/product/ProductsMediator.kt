package com.naiksan.inventorymanager.data.repository.product

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.naiksan.inventorymanager.data.db.AppDatabase
import com.naiksan.inventorymanager.data.db.entities.RemoteKeys
import com.naiksan.inventorymanager.data.db.entities.SearchProductEntity
import com.naiksan.inventorymanager.data.mappers.entity_mapper.ProductMapper
import com.naiksan.inventorymanager.data.network.ApiService
import com.naiksan.inventorymanager.data.network.response.Error
import com.naiksan.inventorymanager.data.network.response.Resource
import com.naiksan.inventorymanager.data.network.response.toError
import java.io.IOException

private const val INITIAL_PAGE_INDEX = 1
private const val TAG = "ProductsMediator"

@ExperimentalPagingApi
class ProductsMediator(
    private val service: ApiService,
    private val db: AppDatabase,
    private val name: String?,
    private val manufacturer: String?,
) : RemoteMediator<Int, SearchProductEntity>() {

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, SearchProductEntity>
    ): MediatorResult {
        try {
            val page = when (loadType) {
                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    remoteKeys?.nextKey?.minus(1) ?: INITIAL_PAGE_INDEX
                }
                LoadType.PREPEND -> {
                    val remoteKeys = getRemoteKeyForFirstItem(state)
                    val prevKey = remoteKeys?.prevKey
                        ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                    prevKey
                }
                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                    val nextKey = remoteKeys?.nextKey
                        ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                    nextKey
                }
            }

            val response = try {
                val response = service.searchProductsAsync(
                    name = name,
                    manufacturer = manufacturer,
                    page = page
                )
                val error = response.error
                val data = response.data
                when {
                    error != null -> Resource.Failure(error)
                    data != null -> Resource.Success(response.data.products)
                    else -> Resource.Failure(Error.NetworkError.Other)
                }
            } catch (e: Exception) {
                e.toError()
            }

            return when (response) {
                is Resource.Failure -> MediatorResult.Error(
                    Throwable(response.error)
                )
                is Resource.Success -> {
                    val products = response.data
                    val endOfPaginationReached = products.isEmpty()

                    db.withTransaction {
                        if (loadType == LoadType.REFRESH) {
                            db.remoteKeysDao().clearRemoteKeys()
                            db.searchProductDao().clear()
                        }
                        val prevKey = if (page == INITIAL_PAGE_INDEX) null else page - 1
                        val nextKey = if (endOfPaginationReached) null else page + 1
                        val keys = products.map {
                            RemoteKeys(repoId = it.id, prevKey = prevKey, nextKey = nextKey)
                        }
                        db.remoteKeysDao().insertAll(keys)
                        db.searchProductDao()
                            .insert(products.map { ProductMapper.dtoToEntity(it) })
                    }

                    return MediatorResult.Success(
                        endOfPaginationReached = endOfPaginationReached
                    )
                }
            }
        } catch (e: IOException) {
            return MediatorResult.Error(e)
        }
    }


    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, SearchProductEntity>): RemoteKeys? {
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { repo ->
                db.remoteKeysDao().remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, SearchProductEntity>): RemoteKeys? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { repo ->
                db.remoteKeysDao().remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, SearchProductEntity>
    ): RemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                db.remoteKeysDao().remoteKeysRepoId(repoId)
            }
        }
    }

}
