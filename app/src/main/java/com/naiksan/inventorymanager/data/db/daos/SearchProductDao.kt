/**
 * @author sanket
 * Created on 21/11/20
 */

package com.naiksan.inventorymanager.data.db.daos

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naiksan.inventorymanager.data.db.entities.SearchProductEntity

/**
 * Searched products are cached into [SearchProductEntity] table
 */
@Dao
interface SearchProductDao {
    @Query("SELECT sp.id, sp.name, sp.manufacturer, sp.sku, sp.unit, sp.unitQty, ip.qty FROM SEARCH_PRODUCT as sp  LEFT JOIN INVENTORY_PRODUCT as ip ON ip.id = sp.id WHERE (sp.name LIKE :name AND sp.manufacturer LIKE :manufacturer)")
    fun getSearch(
        name: String,
        manufacturer: String
    ): PagingSource<Int, SearchProductEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(products: List<SearchProductEntity>): List<Long>

    @Query("DELETE FROM search_product")
    fun clear()

    @Query("DELETE FROM search_product WHERE search_product.id = :id")
    suspend fun delete(id: String)
}
