/**
 * @author sanket
 */

package com.naiksan.inventorymanager.data.db.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "search_product")
data class SearchProductEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id: String,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "manufacturer")
    val manufacturer: String,
    @ColumnInfo(name = "sku")
    val sku: String,
    @ColumnInfo(name = "unitQty")
    val unitQty: Double,
    @ColumnInfo(name = "unit")
    val unit: ProductUnit,
    @ColumnInfo(name = "qty")
    val qty: Int?,
)

@Entity(tableName = "inventory_product")
data class InventoryProductEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id: String,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "manufacturer")
    val manufacturer: String,
    @ColumnInfo(name = "sku")
    val sku: String,
    @ColumnInfo(name = "unitQty")
    val unitQty: Double,
    @ColumnInfo(name = "unit")
    val unit: ProductUnit,
    @ColumnInfo(name = "qty")
    val qty: Int,
)

@Parcelize
enum class ProductUnit(val value: Int) :
    Parcelable { GM(0), KG(1), L(2), ML(3), DZ(4), UNKNOWN(5) }

class UnitConverter {

    @TypeConverter
    fun fromUnit(value: ProductUnit): Int {
        return value.value
    }

    @TypeConverter
    fun toUnit(value: Int): ProductUnit {
        return ProductUnit.values()[value]
    }

    companion object {
        fun productUnitFromString(value: String): ProductUnit {
            return when (value) {
                "gm" -> ProductUnit.GM
                "kg" -> ProductUnit.KG
                "l" -> ProductUnit.L
                "ml" -> ProductUnit.ML
                "dz" -> ProductUnit.DZ
                else -> ProductUnit.UNKNOWN
            }
        }
    }
}