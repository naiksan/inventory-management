/**
 * @author sanket
 * Created on 18/12/20
 */

package com.naiksan.inventorymanager.data.mappers.entity_mapper

interface EntityMapper<Entity, Model> {
    fun fromEntity(entity: Entity): Model

    fun toEntity(model: Model): Entity
}